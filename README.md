# MongoExpressReactNode Starter Project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Usage

You may need to install a local copy of mongodb if you do not already have it.

    npm install mongodb-portable -g --save

It is important that the database is installed globally. After the installation, the path of the binary files will be printed. Ensures that the files from this path can be found. For example, you can expand your PATH variable.

    git clone https://github.com/richard.martens/mern-starter.git
    mkdir ./mongodb
    
    cd mern-starter
    npm run inst-all
    npm start

Change the name of database in file `./api/app.js` or use the environment variable `DATABASE`.

## Components

- Busy indicator
- Alert

## Available Scripts in api folder

In folder `./api`, you can run:

### `npm dev`

Runs the express service in development mode with the root [http://localhost:3001/api](http://localhost:3001/api). The page will reload if you make edits. You can change the port with environment vaiable `PORT`.

### `npm debug`

Run the express service in debug mode with the root [http://localhost:3001/api](http://localhost:3001/api). The page will reload if you make edits. You can change the port with environment vaiable `PORT`.

### `npm start`

Run the express service with the root [http://localhost:3001/api](http://localhost:3001/api). You can change the port with environment vaiable `PORT`.

## Available Scripts in root folder

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
