const i18nCreate = require('express-rest-i18n');
const _ = require('lodash');

const app = require('./app.i18n.js');
const auth = require('./routes/auth.i18n');
const user = require('./routes/user.i18n');
const passport = require('./routes/passport.i18n');

const i18nTranslationFiles = [
	app,
  auth,
  passport,
  user
];

const translations = i18nTranslationFiles.reduce(_.merge);

const i18n = i18nCreate({
  defaultLocale: 'en',
  warn: false, // optional
  allowFallback: true, // optional
  messages: translations
});

module.exports = i18n;