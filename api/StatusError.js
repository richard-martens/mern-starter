function StatusError(status, message, fileName, lineNumber) {
  var instance = new Error(message, fileName, lineNumber);

  instance.status = status;

  if(typeof Object.setPropertyOf != 'undefined') {
    Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
  } else {
    StatusError.__proto__ = Object.getPrototypeOf(this);
  }

  if(Error.captureStackTrace) {
    Error.captureStackTrace(instance, StatusError);
  }
  return instance;
}

StatusError.prototype = Object.create(Error.prototype, {
  constructor: {
    value: Error,
    enumerable: false,
    writable: true,
    configurable: true
  }
});

if(typeof Object.setPropertyOf != 'undefined') {
  Object.setPrototypeOf(StatusError, Error);
} else {
  StatusError.__proto__ = Error;
}

module.exports = StatusError;