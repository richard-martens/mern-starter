var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var sassMiddleware = require('node-sass-middleware');
var i18n = require('./i18n');
var router = require('./routes/index');

var mongoose = require('mongoose');
mongoose.connect(process.env.DATABASE || 'mongodb://localhost:27017/mern_starter');

var app = express();
var port = process.env.PORT || 3001;

app.set('port', port);
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(i18n.middleware);

app.use('/api/auth', router.Auth);
app.use('/api/user', router.User);

// Ressource not found error handler
app.use(function (req, res) {
  res.statusMessage = res.i18n.t('Resource_not_found');

  res.status(404).end();
});

// Transform any errors message to the Status-Text
app.use(function (err, req, res, next) {
  res.statusMessage = res.i18n.t(err.message);
  res.status(err.status || 500).end();
});

app.listen(port, () => {
  console.log(`Express server listening on port ${port}`);
})
