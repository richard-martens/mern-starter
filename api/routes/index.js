const authRouter = require('./auth.router');
const userRouter = require('./user.router');

module.exports = {
  Auth: authRouter,
  User: userRouter
};
