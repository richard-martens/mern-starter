module.exports = {
	'en': { // locale
		'passport': {
			'Login_failed': 'Login failed'
		}
	},

	'de': { // locale
		'passport': {
			'Login_failed': 'Anmeldung misslungen'
		}
	}
};