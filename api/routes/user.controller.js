
const User = require('./user.model');

function cleanUser(user, soft) {
  let result = user.toObject();

  delete result.password;
  if (!soft) {
    delete result.email;
  }

  return result;
}

/**
 * getUsers  - Returns JSON for all users
 * @returns {Array} - Array of users
 */
exports.getUsers = async (req, res, next) => {
  try {
    const users = await User.find({});

    res.status(200).json({
      users: users.map(user => cleanUser(user))
    });

  } catch (err) {
    return next(err);
  }
};

exports.getCurrent = async (req, res, next) => {
  try {
    res.status(200).json(cleanUser(req.user, true));

  } catch (err) {
    return next(err);
  }
}

exports.putCurrent = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.user._id });

    user.email = req.body.email;
    user.name.first = req.body.name.first;
    user.name.last = req.body.name.last;

    await user.save();

    res.status(204).end();

  } catch (err) {
    return next(err);
  }
}

exports.putPassword = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.user._id });


    user.comparePassword(req.body.currentPassword, async (err, isMatch) => {
      if (!isMatch) {
        res.statusMessage = res.i18n.t('user.wrong_password');
        res.status(400).end();
        return;
      }

      user.password = req.body.password;

      await user.save();

      res.status(204).end();
    });

  } catch (err) {
    return next(err);
  }
};

exports.deleteCurrent = async (req, res, next) => {
  try {
    await User.deleteOne({ _id: req.user._id });

    res.status(204).end();

  } catch (err) {
    return next(err);
  }
};