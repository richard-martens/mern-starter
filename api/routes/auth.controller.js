
const User = require('./user.model');
const StatusError = require('../StatusError');
const jwt = require('jsonwebtoken');

function setCurrentUser(user, res) {
  let result = user.toObject();

  delete result.password;
  delete result.email;

  const token = jwt.sign(result, process.env.SECRET || 'super secret passphrase', {
    expiresIn: process.env.JWT_EXPIRATION || 604800,
  });

  res.status(200).json({
    user: result,
    token: `JWT ${token}`,
    expiration: process.env.JWT_EXPIRATION || 604800, 
  });
}

function onError(err, res) {
  if (err.errors) {
    const invalidFields = Object.keys(err.errors);

    res.statusMessage = err.name;
    res.status(400).send({
      invalidFields: invalidFields.map(field => ({
        field: field,
        message: err.errors[field].kind === 'user defined' ? res.i18n.t(err.errors[field].message) : err.errors[field].message
      }))
    });

  } else if(err.status) {
    res.statusMessage = res.i18n.t(err.message);
    res.status(err.status).end();

  } else {
    return next(err);

  }
}

exports.login = async (req, res, next) => {
  try {
    setCurrentUser(req.user, res);

  } catch (err) {
    onError(err, res);
  }
};

exports.register = async (req, res, next) => {
  try {
    if(!req.body) {
      throw new StatusError(400, res.i18n.t('auth.Attribute_{0}_not_passed').replace('{0}', 'body'));
    }

    const { email, password, name } = req.body;

    let user = null;

    if (email) {
      user = await User.findOne({ email: email.toLowerCase() });
    }

    if (user !== null) {
      res.statusMessage = res.i18n.t('auth.This_email_address_is_already_registered');
      res.status(422).end();

    } else {
      user = new User({
        name,
        email,
        password
      });

      let savedUser = await user.save();

      setCurrentUser(savedUser, res);

    }

  } catch (err) {
    onError(err, res);
  }
};
