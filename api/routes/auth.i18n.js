module.exports = {
	'en': { // locale
		'auth': {
			'Attribute_{0}_not_passed': 'Attribute {0} not passed',
			'This_email_address_is_already_registered': 'It must contain at least one capital letter'
		}
	},

	'de': { // locale
		'auth': {
			'Attribute_{0}_not_passed': 'Attribut {0} nicht übergeben',
			'This_email_address_is_already_registered': 'Diese Email-Adresse ist bereits registriert'
		}
	}
};