var express = require('express');
var router = express.Router();
var controller = require('./auth.controller');
const passport = require('./passport');

router.post('/login', passport.requireLogin, controller.login);
router.post('/register', controller.register);

module.exports = router;
