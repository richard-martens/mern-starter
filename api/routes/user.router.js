var express = require('express');
var router = express.Router();
var controller = require('./user.controller');
const passport = require('./passport');

router.put('/current/password', passport.requireAuth, controller.putPassword);

router.get('/current', passport.requireAuth, controller.getCurrent);
router.put('/current', passport.requireAuth, controller.putCurrent);
router.delete('/current', passport.requireAuth, controller.deleteCurrent);

router.get('/', passport.requireAuth, controller.getUsers);

module.exports = router;
