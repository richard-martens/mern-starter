const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcrypt');

const Schema = mongoose.Schema;

//= ===============================
// User Schema
//= ===============================
const UserSchema = new Schema({
  email: {
    type: String,
    lowercase: true,
    unique: true,
    required: true,
    validate: [validator.isEmail, 'user.This_is_not_a_valid_email_address']
  },
  password: { 
    type: String, 
    required: true,
    minLength: 8,
    match: [/[a-z]+/, 'user.It_must_contain_at_least_one_lowercase_letter'],
    validate: [{
      validator: (value) => value.match(/[A-Z]+/),
      msg: 'user.It_must_contain_at_least_one_capital_letter'
    }, {
      validator: (value) => value.match(/[0-9]+/),
      msg: 'user.It_must_contain_at_least_one_number'
    }, {
      validator: (value) => value.match(/[^a-zA-Z0-9]+/),
      msg: 'user.It_must_contain_at_least_one_special_character'
    }]
  },
  name: {
    first: { type: String, required: true },
    last: { type: String, required: true },
  },
  deactivated: { type: Boolean, default: false },
},
{
  timestamps: true,
  toObject: {
    virtuals: true,
  },
  toJSON: {
    virtuals: true,
  },
});

// Method to compare password for login
UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) { return cb(err); }

    cb(null, isMatch);
  });
};

//= ===============================
// User model hooks
//= ===============================
async function hashPassword(next) {
  const user = this;

  if (user && user.isModified('password')) {
    try {
      const salt = await bcrypt.genSalt(5);
      
      user.password = await bcrypt.hash(user.password, salt, null);

      return next();

    } catch (err) {
      return next(err);

    }

  } else {
    return next();

  }

}

// Pre-save of user to database, hash password if password is modified or new
UserSchema.pre('save', hashPassword);
UserSchema.pre('update', hashPassword);

module.exports = mongoose.model('User', UserSchema);