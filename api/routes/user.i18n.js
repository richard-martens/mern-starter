module.exports = {
	'en': { // locale
		'user': {
			'It_must_contain_at_least_one_capital_letter': 'It must contain at least one capital letter',
			'It_must_contain_at_least_one_lowercase_letter': 'It must contain at least one lowercase letter',
			'It_must_contain_at_least_one_number': 'It must contain at least one number',
			'It_must_contain_at_least_one_special_character': 'It must contain at least one special character',
			'This_is_not_a_valid_email_address': 'This is not a valid email address',
			'wrong_password': 'Invalid password'
		}
	},

	'de': { // locale
		'user': {
			'It_must_contain_at_least_one_capital_letter': 'Es muss mindestens ein Großbuchstabe enthalten sein',
			'It_must_contain_at_least_one_lowercase_letter': 'Mindestens ein kleinbuchstabe muss enthalten sein',
			'It_must_contain_at_least_one_number': 'Es muss mindestens eine Ziffer enthalten sein',
			'It_must_contain_at_least_one_special_character': 'Es muss mindestens ein Sonderzeichen enthalten sein',
			'This_is_not_a_valid_email_address': 'Das ist keine gültige Email-Adresse',
			'wrong_password': 'Kennwort ist falsch'
		}
	}
};