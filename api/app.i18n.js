// messages.js file
module.exports = {
	'en': { // locale
		'Resource_not_found': 'Resource not found',
	},

	'de': { // locale
		'Resource_not_found': 'Ressource nicht gefunden',
	}
};