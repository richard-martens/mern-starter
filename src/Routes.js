import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './components/pages/Home';
import Register from './components/pages/Register';
import NotFound from './components/pages/NotFound';
import PrivateRoute from './components/common/PrivateRoute';
import Dashboard from './components/pages/Dashboard';
import Login from './components/pages/Login';
import Profile from './components/pages/Profile';

const TopLevelRoutes = () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Route path="/login" component={Login} />
    <PrivateRoute path="/dashboard" component={Dashboard} />
    <Route path="/register" component={Register} />
    <PrivateRoute path="/profile" component={Profile} />
    <Route path="*" component={NotFound} />
  </Switch>
);

export default TopLevelRoutes;
