
import { getComponentTranslator } from '../i18n';
const translate = getComponentTranslator('User');

const USER = {
  firstName: {
    input: {
      name: 'name.first', 
      type: 'text', 
      required: true
    },
    label: translate('Firstname'), 
  }, 
  lastName: {
    input: {
      name: 'name.last', 
      type: 'text', 
      required: true
    },
    label: translate('Lastname')
  }, 
  email: {
    input: {
      name: 'email', 
      type: 'email', 
      required: true
    },
    label: translate('Email')
  }, 
  password: {
    input: {
      name: 'password', 
      type: 'password', 
      required: true, 
      minLength: 8,
      onValidate: e => {
        const passwordError = USER.password.validate(e.target.value);
        
        if((e.target.validity.customError && e.target.validationMessage !== passwordError)
            || (e.target.validity.valid && !passwordError)) {
          e.target.setCustomValidity(passwordError);
          e.target.checkValidity();
        }
      }
    },
    label: translate('Password'),
    validate: value => {
      if (!value.match(/[a-z]+/)) {
        return translate('It must contain at least one lowercase letter');
  
      } else if (!value.match(/[A-Z]+/)) {
        return translate('It must contain at least one capital letter');
  
      } else if (!value.match(/[0-9]+/)) {
        return translate('It must contain at least one number');
  
      } else if (!value.match(/[^a-zA-Z0-9]+/)) {
        return translate('It must contain at least one special character');
  
      }
      return '';
    }
  }, 
  passwordRepeat: {
    input: {
      name: 'passwordRepeat', 
      type: 'Password', 
      required: true, 
      minLength: 8,
      onValidate: e => USER.password.input.onValidate(e)
    },
    label: translate('Password repeat'),
    validate: (password, passwordRepeat) => {
      if (password !== passwordRepeat) {
        return translate("Password and repeated password do not match");
  
      } 
      return '';
    }
  },
};

export default USER;