const translation = {
	de: {
		User: {
			"Email": "Email",
			"Firstname": "Vorname",
			'It must contain at least one capital letter': 'Es muss mindestens ein Großbuchstabe enthalten sein',
			'It must contain at least one lowercase letter': 'Mindestens ein kleinbuchstabe muss enthalten sein',
			'It must contain at least one number': 'Es muss mindestens eine Ziffer enthalten sein',
			'It must contain at least one special character': 'Es muss mindestens ein Sonderzeichen enthalten sein',
			"Lastname": "Nachname",
			"Password": "Kennwort",
			"Password repeat": "Kennwort wiederholen",
			"Password and repeated password do not match": "Kennwort und Kennwort-Wiederholung stimmen nicht überein"
		}
	},
	en: {
		User: {
			"Email": "Email",
			"Firstname": "Firstname",
			"It must contain at least one capital letter": "It must contain at least one capital letter",
			"It must contain at least one lowercase letter": "It must contain at least one lowercase letter",
			"It must contain at least one number": "It must contain at least one number",
			"It must contain at least one special character": "It must contain at least one special character",
			"Lastname": "Lastname",
			"Password": "Password",
			"Password repeat": "Password repeat",
			"Password and repeated password do not match": "Password and repeated password do not match"
		}
	}
};


export default translation;