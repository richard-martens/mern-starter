import { translate, getTranslations, setLocale, setTranslations } from 'react-i18nify';
import _ from 'lodash';

const getClientLanguage = () => {
	const translations = getTranslations(),
		translatedlanguages = Object.keys(translations);
	let translatedRegex,
		clientLanguage;

	switch (translatedlanguages.length) {
		case 0:
			return undefined;

		case 1:
			translatedRegex = new RegExp(`^${translatedlanguages[0]}`);
			break;

		default:
			translatedRegex = translatedlanguages.reduce((accumulator, currentValue, index) =>
				index === 1 ? `(${accumulator})|(${currentValue})` : `${accumulator}|(${currentValue})`
			);
			translatedRegex = new RegExp(`^${translatedRegex}`);
	}

	if (navigator.languages) {
		clientLanguage = navigator.languages.find(
			language => translatedRegex.test(language));

		if (clientLanguage) {
			return translations[clientLanguage] ? clientLanguage : clientLanguage.substring(0, 2);
		}
	}

	clientLanguage = navigator.language || navigator.userLanguage;

	if (clientLanguage && translatedlanguages.find(language => clientLanguage.search(language) >= 0 || language.search(clientLanguage) >= 0)) {
		return translations[clientLanguage] ? clientLanguage : clientLanguage.substring(0, 2);
	}

	return undefined;
}

export const getComponentTranslator = (name) => {
	return (key, parameters) => {
		const escapedKey = key.replace(/[.]/g, "&#002E;");
		const translation = translate(name + '.' + escapedKey, parameters);

		return translation.replace(/&#002E;/g, ".");
	}
};

const i18nTranslationFiles = [
	require('./components/pages/NotFound.i18n').default,
	require('./components/App.i18n').default,
	require('./components/pages/Register.i18n').default,
	require('./components/pages/Login.i18n').default,
	require('./model/user.i18n').default,
	require('./components/pages/Dashboard.i18n').default,
	require('./components/pages/Profile.i18n').default
];

const translations = i18nTranslationFiles.reduce(_.merge);

export const getLocales = () => Object.keys(translations);

setTranslations(translations);
setLocale(getClientLanguage() || 'en');