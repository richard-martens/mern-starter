import { Icon } from '@ui5/webcomponents-react';

import './Tile.scss';

export default function Tile(props) {
  return (
    <button className="rm-tile" onClick={() => props.history.push(props.to)}>
      <div className="rm-tile-header">
        <h2>{props.titleText}</h2>
        {props.subTitle
          && <h3>{props.subTitle}</h3>}
      </div>

      <div className="rm-tile-value-container">
        {props.icon 
          && <Icon name={props.icon} />}
        {props.value !== undefined && props.value !== null
          && <span className="rm-tile-value">{props.value}</span>}
      </div>
      
      {props.statusText
          && <span className="rm-tile-status-text">{props.statusText}</span>}
          
    </button>
  );
}