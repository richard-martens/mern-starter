import { useEffect, useRef, useState } from 'react';
import './Input.scss';

export default function Input(props) {
  const [message, setMessage] = useState('');
  const ref = useRef();
  const sync = e => {
    if (props.onValidate) {
      props.onValidate(e);
    }
    setMessage(e.target.validationMessage);
  };

  let className;
  switch (props.design) {
    case 'Negative':
      className = 'rm-input rm-input-negative';
      break;

    default:
      className = 'rm-input rm-input-default';
      break;
  }

  const nativeProps = {...props};
  delete nativeProps.onValidate;

  const events = {
    onInvalid: sync,
    onBlur: sync,
    onInput: sync
  };

  useEffect(() => {
    setMessage(ref.current.validationMessage);
  }, [ref])

  return (
    <div className={className}>
      <input {...nativeProps} {...events} ref={ref}>
        {props.children}
      </input>
      <div className="rm-invalid-feedback">
        {message}
      </div>
    </div>);
}