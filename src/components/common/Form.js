import { Component } from 'react';
import './Form.scss';
import { NAME } from '../../other/FormValidationError';
import { BusyIndicator, Form as UI5Form, FormItem, MessageStrip } from '@ui5/webcomponents-react';
import Input from './Input';


export default class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      apiError: null,
      busy: false,
      touched: false
    };
  }

  onSubmit = async e => {
    e.preventDefault();

    const inputs = Array.prototype.slice.call(e.target.elements).filter(element => element.willValidate);

    try {

      if (this.props.onCustomValidation) {
        this.props.onCustomValidation(e);
      }

      this.setState({
        touched: true
      });

      if (e.target.reportValidity()) {
        this.setState({
          busy: true,
          apiError: null
        });

        await this.props.onSubmit(e);

      }

    } catch (error) {
      // assign field Errors to specified fields
      if (error.name === NAME) {
        error.fields.forEach(field => {
          const input = inputs.find(input => input.name === field.field);

          if (input) {
            input.setCustomValidity(field.message);
            input.checkValidity();
          }
        });

      } else {
        // assign custom error, not specific to any field    
        this.setState(() => ({
          apiError: error.message
        }));

      }
    } finally {
      this.setState(() => ({
        busy: false
      }));
    }
  }

  render() {
    return (
      <BusyIndicator className={this.state.touched ? 'rm-form rm-form-touched' : 'rm-form'} delay={0} active={this.state.busy} >
        <form onSubmit={this.onSubmit} noValidate>
          <UI5Form as="div" titleText={this.props.titleText} columnsXL={this.props.columnsXL ? this.props.columnsXL : 2} >
            {this.props.children}
            {this.state.apiError &&
              <FormItem label="">
                <MessageStrip design="Negative" hideCloseButton={true}>{this.state.apiError}</MessageStrip>
              </FormItem>}
            {this.props.submit &&
              <FormItem label="">
                <Input type="submit" value={this.props.submit} design={this.props.design} />
              </FormItem>}

          </UI5Form>
        </form>
      </BusyIndicator>);
  }
}