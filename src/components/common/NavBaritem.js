import { Link } from 'react-router-dom';
import { useState } from 'react';
import './NavBarItem.scss';

export default function NavBarItem(props) {
	const [collapsed, setCollapsed] = useState(true);
	const className = props.className ? `rm-navbar-item ${props.className}` : 'rm-navbar-item';

	const getClickHandler = (props_param, bubble) => {
		return () => {
			// for dropdown in dropdown -> send close event to parent
			if (bubble && props_param.onClick) {
				props_param.onClick();
			}
			setCollapsed(!collapsed);
		}
	};
	
	if (props.items) {
		const items = props.items.map(item => (
			item && <NavBarItem key={item.key} {...item.props} onClick={getClickHandler(item.props, true)} />
		));

		return (
			<div className={className + ' rm-navbar-sub-container'}>
				<button className="rm-navbar-toggler" onClick={getClickHandler(props)} >
					{props.float === 'left' ? <span className="ui5-shellbar-menu-button-arrow"></span> : null}
					{props.text}
					{props.float === 'right' ? <span className="ui5-shellbar-menu-button-arrow"></span> : null}
				</button>
					<div className={collapsed ? 'rm-navbar-sub-collapsed' : 'rm-navbar-sub'}>{items}</div>
			</div>);

	} else if (props.to) {
		return (<Link className={className} {...props}>{props.text}</Link>);

	} else {
		return (
			<button className={className} {...props} onClick={getClickHandler(props, true)} >{props.text}</button>
		);
	}

}