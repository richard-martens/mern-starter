import './NavBar.scss';
import NavBarItem from './NavBaritem';

export default function NavBar(props) {
	let items = [].concat(props.itemsLeft);

	items = items.concat(props.itemsRight);

	//<DropDownMenu text="&#x2630;" items={props.items} toggler="right" />
	return (
		<div className="rm-navbar">
			<div className="rm-navbar-left">
				{props.brandText && <span>{props.brandText}</span>}
				{props.itemsLeft}
			</div>
			<div className="rm-navbar-right">
				{props.itemsRight}
			</div>
			{props.brandText && <span className="rm-navbar-mobile">{props.brandText}</span>}
			<NavBarItem className="rm-navbar-mobile" text="&#x2630;" items={items} />
		</div>
	);
}