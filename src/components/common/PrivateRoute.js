import { useContext } from 'react';
import { Redirect, Route, useLocation } from 'react-router-dom';
import { AuthContext } from '../App';

const PrivateRoute = props => {
  const location = useLocation();
  const authLogin = useContext(AuthContext);

  return authLogin.user ? (
    <Route {...props} />
  ) : (
    <Redirect
      to={{
        pathname: "/login",
        state: { from: location }
      }}
    />
  );
};

export default PrivateRoute;