import Routes from '../Routes';
import { getComponentTranslator, getLocales } from '../i18n';
import { getLocale, setLocale } from 'react-i18nify';
import NavBar from './common/NavBar';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { ThemeProvider } from '@ui5/webcomponents-react';
import '@ui5/webcomponents-icons/dist/line-chart.js';
import NavBarItem from './common/NavBaritem';

const translate = getComponentTranslator('App');

export const AuthContext = React.createContext({});

class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			currentLocale: getLocale(),
			authContext: {
				user: sessionStorage.getItem('user') && JSON.parse(sessionStorage.getItem('user')),
				setUser: (data) => {
					if (!data || !data.user || !data.user.name) {
						throw new Error(translate('Invalid User'))
					}
					sessionStorage.setItem('user', JSON.stringify(data.user));
					if(data.token && data.expiration) {
						sessionStorage.setItem('token', data.token);
						sessionStorage.setItem('expiration', data.expiration);
					}
					
					this.setState({
						authContext: {
							...this.state.authContext,
							user: data.user
						}
					});
				},
				logout: () => {
					sessionStorage.clear();
					this.setState({
						authContext: {
							...this.state.authContext,
							user: null
						}
					});
					this.props.history.push('/');
				}
			}
		};
	}

	render() {
		const locales = getLocales();

		const left = (
			<>
				<NavBarItem key='home' text={translate('Home')} to="/" />
				{!this.state.authContext.user
					&& <NavBarItem key='register' text={translate('Register')} to='/register' hidden={this.state.authContext.user ? true : false} />}
				{this.state.authContext.user
					&& 	<NavBarItem key='dashboard' text={translate('Dashboard')} to='/dashboard' hidden={this.state.authContext.user ? true : false} />}
			</>);
		const float = {
			float: 'right'
		};
		
		const right = (
			<>
				{!this.state.authContext.user
					&& <NavBarItem key='login' text={translate('Login')} to='/login' hidden={this.state.authContext.user ? true : false} />}
				{this.state.authContext.user
					&& <NavBarItem key='logout' text={translate('Logout', this.state.authContext.user ? this.state.authContext.user.name : null)}
						onClick={this.state.authContext.logout} />}
				<NavBarItem key={this.state.currentLocale} text={translate(this.state.currentLocale)} {...float}
					items={locales.map(locale => {// items defines as sub menu
						return locale !== this.state.currentLocale ?
							<NavBarItem key={locale} text={translate(locale)} onClick={() => {
								setLocale(locale);
								this.setState({
									currentLocale: locale
								});
							}} /> : null;
					})} />
			</>
		);

		return (
			<ThemeProvider>
				<AuthContext.Provider value={this.state.authContext}>
					<NavBar brandText="Mern Starter" itemsLeft={left.props.children} itemsRight={right.props.children} />
					<Routes />
				</AuthContext.Provider>
			</ThemeProvider>
		);
	}

}

export default withRouter(App);