const translation = {
	de: {
		Dashboard: {
			"Dashboard": "Dashboard",
			"My profile": "Mein Profil"
		}
	},
	en: {
		Dashboard: {
			"Dashboard": "Dashboard",
			"My profile": "Mein Profil"
		}
	}
};


export default translation;