import Tile from '../common/Tile';
import "@ui5/webcomponents-icons/dist/account.js";
import { Title } from '@ui5/webcomponents-react';
import './Dashboard.scss';
import { getComponentTranslator } from '../../i18n';

const translate = getComponentTranslator('Dashboard');

export default function Dashboard(props) {
  return (
    <div className="rm-dashboard">
      <Title level="H1">{translate('Dashboard')}</Title>
      <div className="rm-dashboard-content">
        <Tile history={props.history} to="/profile" titleText={translate('My profile')} icon="account" />
      </div>
    </div>
    );
}
