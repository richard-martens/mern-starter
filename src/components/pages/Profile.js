import "@ui5/webcomponents-icons/dist/account.js";
import { Title } from '@ui5/webcomponents-react';
import './Profile.scss';
import { getComponentTranslator } from '../../i18n';
import Form from '../common/Form';
import React from 'react';
import USER from '../../model/user';
import { FormItem, MessageStrip } from '@ui5/webcomponents-react';
import Input from '../common/Input';
import { httpDelete, get, put } from '../../other/http';
import { AuthContext } from '../App';

const translate = getComponentTranslator('Profile');

export default class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      currentPassword: '',
      password: '',
      passwordRepeat: '',
      error: null
    };
    
    Profile.contextType = AuthContext;
  }

  componentDidMount() {
    const init = async () => {
      try {
        const user = await get('/api/user/current');

        this.setState({
          firstName: user.name.first,
          lastName: user.name.last,
          email: user.email
        });

      } catch (error) {
        this.setState({
          error: error.message
        });
      }
    }

    init();
  }

  onUpdateProfile = async e => {
    await put('/api/user/current', {
      name: {
        first: this.state.firstName,
        last: this.state.lastName
      },
      email: this.state.email
    });

    this.context.setUser({
      user: {
        ...this.context.user,
        name : {
          first: this.state.firstName,
          last: this.state.lastName
        },
        email: this.state.email
      }
    });
  }

  onChangePassword = async e => {
    await put('/api/user/current/password', {
      currentPassword: this.state.currentPassword,
      password: this.state.password
    });
  }

  onChangePasswordValidation = e => {
    // custom validation
    const passwordResetError = USER.passwordRepeat.validate(this.state.password, this.state.passwordRepeat);
    e.target.elements.passwordRepeat.setCustomValidity(passwordResetError);
    e.target.elements.passwordRepeat.checkValidity();
  }

  deleteAccount = async e => {
    await httpDelete('/api/user/current');

    this.context.logout();
  }

  render() {
    const keysData = ['firstName', 'lastName', 'email'];
    const keysPassword = ['password', 'passwordRepeat'];

    return (
      <div className="rm-profile">
        <Title level="H1">{translate('My profile')}</Title>
        <div className="rm-profile-content">
          <Form columnsXL={1} titleText={translate('My data')} onSubmit={this.onUpdateProfile}
            submit={translate('Change')} >
            {keysData.map(key =>
              <FormItem key={key} label={USER[key].label}>
                <Input {...USER[key].input} value={this.state[key]} onChange={(e) => {
                  this.setState({ [key]: e.target.value });
                }} />
              </FormItem>
            )}
            {this.state.error
              && <FormItem label="">
                <MessageStrip design="Negative" hideCloseButton={true}>{this.state.error}</MessageStrip>
              </FormItem>}
          </Form>

          <Form columnsXL={1} titleText={translate('Change Password')} onSubmit={this.onChangePassword}
            submit={translate('Change')} onCustomValidation={this.onChangePasswordValidation} >
            <FormItem label={translate('Current password')}>
              <Input {...USER.password.input} name="currentPassword" value={this.state.currentPassword} onChange={(e) => {
                this.setState({ currentPassword: e.target.value });
              }} />
            </FormItem>
            {keysPassword.map(key =>
              <FormItem key={key} label={USER[key].label}>
                <Input {...USER[key].input} value={this.state[key]} onChange={(e) => {
                  this.setState({ [key]: e.target.value });
                }} />
              </FormItem>
            )}
          </Form>

          <Form columnsXL={1} titleText={translate('Delete account')} onSubmit={this.deleteAccount} 
            submit={translate('Delete')} design="Negative" >
          </Form>
          
        </div>
      </div>
    );
  }
}