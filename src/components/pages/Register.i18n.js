const translation = {
	de: {
		Register: {
			"Dashboard": "Dashboard",
			"Register": "Registrieren",
			"Submit": "Absenden"
		}
	},
	en: {
		Register: {
			"Dashboard": "Dashboard",
			"Register": "Register",
			"Submit": "Submit"
		}
	}
};


export default translation;