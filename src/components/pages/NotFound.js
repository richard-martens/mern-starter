import { getComponentTranslator } from '../../i18n';
import { IllustratedMessage } from '@ui5/webcomponents-react';
import "@ui5/webcomponents-fiori/dist/illustrations/PageNotFound.js"

import './NotFound.scss';

const translate = getComponentTranslator('NotFound');

export default function NotFoundPage(props) {
	return (
	<div className="rm-not_found">
		<IllustratedMessage titleText={translate('404 - Page Not Found')} name="PageNotFound"
			subtitleText={translate('I\'m sorry, the page you were looking for cannot be found!')} />
	</div>
	);
}