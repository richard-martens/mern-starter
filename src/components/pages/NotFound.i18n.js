const translation = {
	de: {
		NotFound: {
			"404 - Page Not Found": "404 - Seite nicht gefunden",
			"I'm sorry, the page you were looking for cannot be found!": "Die von Ihnen gesuchte Seite wurde leider nicht gefunden!"
		}
	},
	en: {
		NotFound: {
			"404 - Page Not Found": "404 - Page Not Found",
			"I'm sorry, the page you were looking for cannot be found!": "I'm sorry, the page you were looking for cannot be found!"
		}
	}
};

export default translation;