import React from 'react';
import { getComponentTranslator } from '../../i18n';
import { post } from '../../other/http';
import { AuthContext } from '../App';
import USER from '../../model/user';
import Form from '../common/Form';
import { FormItem } from '@ui5/webcomponents-react';
import Input from '../common/Input';

import './Register.scss';

const translate = getComponentTranslator('Register');

export default class Register extends React.Component {

  static contextType;

  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      passwordRepeat: ''
    };

    Register.contextType = AuthContext;
  }

  render() {
    const keys = Object.keys(USER);

    return (
      <div className="rm-register">
        <Form columnsXL={1} titleText={translate('Register')} onSubmit={this.onSubmit}
          onCustomValidation={this.onCustomValidation} submit={translate('Submit')} >
          {keys.map(key =>
            <FormItem key={key} label={USER[key].label}>
              <Input {...USER[key].input} value={this.state[key]} onChange={(e) => {
                this.setState({ [key]: e.target.value });
              }} />
            </FormItem>
          )}
        </Form>
      </div>
    );
  }

  onCustomValidation = e => {
    // custom validation
    let passwordError = USER.passwordRepeat.validate(this.state.password, this.state.passwordRepeat);
    e.target.elements.password.setCustomValidity(passwordError);
    e.target.elements.password.checkValidity();

    passwordError = USER.password.validate(this.state.password);
    e.target.elements.password.setCustomValidity(passwordError);
    e.target.elements.password.checkValidity();
  }

  onSubmit = async (e) => {
    const data = await post('/api/auth/register', {
      name: {
        first: this.state.firstName,
        last: this.state.lastName
      },
      email: this.state.email,
      password: this.state.password
    });

    this.context.setUser(data);
    this.props.history.push('/dashboard');
  }

}
