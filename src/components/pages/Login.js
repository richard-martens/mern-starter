import React from 'react';
import { AuthContext } from '../App';
import { getComponentTranslator } from '../../i18n';
import USER from '../../model/user';
import Form from '../common/Form';
import { FormItem } from '@ui5/webcomponents-react';
import Input from '../common/Input';
import { post } from '../../other/http';

import './Login.scss';

const translate = getComponentTranslator('Login');

export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: ''
    };

    Login.contextType = AuthContext;
  }

  componentDidMount() {
    if (this.context.user) {
      const { from } = this.props.location.state || { from: { pathname: "/dashboard" } };
      this.props.history.replace(from);
    }
  }

  render() {
    const keys = ['email', 'password'];

    return (
      <div className="rm-login">
        <Form titleText={translate('Login')} submit={translate('Login')} onSubmit={this.onSubmit} >
          {keys.map(key =>
            <FormItem key={key} label={USER[key].label}>
              <Input {...USER[key].input} value={this.state[key]} onChange={(e) => {
                this.setState({ [key]: e.target.value });
              }} />
            </FormItem>
          )}
        </Form>
      </div>
    );
  }

  onSubmit = async (e) => {
    const data = await post('/api/auth/login', {
      email: this.state.email,
      password: this.state.password
    });

    this.context.setUser(data);
    this.props.history.push('/dashboard');
  }

}