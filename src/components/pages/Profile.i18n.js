const translation = {
	de: {
		Profile: {
			"Change": "Ändern",
			"Change Password": "Kennwort ändern",
			"Current password": "Aktuelles Kennwort",
			"Delete": "Löschen",
			"Delete account": "Konto löschen",
			"My data": "Meine Daten",
			"My profile": "Mein Profil"
		}
	},
	en: {
		Profile: {
			"Change": "Change",
			"Change Password": "Change Password",
			"Current password": "Current password",
			"Delete": "Delete",
			"Delete account": "Delete account",
			"My data": "My data",
			"My profile": "Mein Profil"
		}
	}
};


export default translation;