const translation = {
	"en": {
		"App": {
			"en": "English",
			"de": "German",
			"Home": "Home",
			"Invalid User": "Invalid User",
			"Login": "Login",
			"Logout": "Log off %{first} %{last}",
			"Register": "Register"
		}
	},
	"de": {
		"App": {
			"en": "Englisch",
			"de": "Deutsch",
			"Home": "Startseite",
			"Invalid User": "Invalid User",
			"Login": "Anmelden",
			"Logout": "%{first} %{last} abmelden",
			"Register": "Registrieren"
		}
	}
}

export default translation;