import { getLocale } from 'react-i18nify';
import { FormValidationError } from '../other/FormValidationError';

const handleResult = async (response) => {
  let jsonResponse;

  if (response.status === 204) {
    return;
  }

  if (response.status < 400) {
    jsonResponse = await response.json();

    return jsonResponse;

  }
  if (response.status === 400 && response.statusText === 'ValidationError') {
    jsonResponse = await response.json();

    throw new FormValidationError(jsonResponse);

  } else {
    throw new Error(response.statusText);

  }
}

const send = async (uri, parameter) => {
  const response = await fetch(uri, {
    ...parameter,
    headers: {
      'application-language': getLocale(),
      'Authorization': sessionStorage.getItem('token'),
      ...parameter.headers
    }
  });

  return handleResult(response);
};

const sendBody = async (method, uri, data) => {
  return await send(uri, {
    method: method,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
};

export const get = async (uri) => {
  return await send(uri, { method: 'GET' });
}

export const post = async (uri, data) => {
  return await sendBody('POST', uri, data);
}

export const put = async (uri, data) => {
  return await sendBody('PUT', uri, data);
} 

export const httpDelete = async (uri) => {
  return await send(uri, { method: 'DELETE' });
}