export const NAME = "ValidationError";

export class FormValidationError extends Error {
  constructor(fields) {
    super(NAME);
    this.name = NAME;
    this.fields = fields;
  }
}